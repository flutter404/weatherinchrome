import 'package:flutter/material.dart';

void main() {
  runApp(weatherApp());
}

class weatherApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
      ),
    );
  }
}

AppBar buildAppBarWidget() {
  return AppBar(
    backgroundColor: Colors.transparent,
    elevation: 0,
    title: Text(
      "ChonBuri",
      style: TextStyle(fontSize: 30),
    ),
    centerTitle: true,
  );
}

Widget buildBodyWidget() {
  return Stack(
    children: <Widget>[
      Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Colors.blue,
            image: DecorationImage(
                image: new NetworkImage(
                    "https://external-preview.redd.it/JI_xq_yzYB2iAGfZ_nWV0pw6WHIHnYsd7Oi-GmDMRgQ.jpg?auto=webp&s=2e24ad0998e441540e3a5ec8f84f07cfce6ea630"),
                fit: BoxFit.fill),
          ),
          child: ListView(
            children: <Widget>[
              buildtemp(),
              buildHourly(),
              build10Day(),
            ],
          )),
    ],
  );
}

Widget buildtemp() {
  return Container(
      height: 170,
      child: ListTile(
        title: Text(
          "25°",
          style: TextStyle(
            fontSize: 80,
            color:Colors.white,
          ),
          textAlign: TextAlign.center,
        ),
        subtitle: Text(
          "Clear\nH:32°  L:21°",
          style: TextStyle(
              fontSize: 20,
              color: Colors.white
          ),
          textAlign:TextAlign.center,
        ),
      )
  );
}

Widget buildHourly() {
  return Container(
      margin: EdgeInsets.all(5),
      decoration: ShapeDecoration(
        color:Colors.black38,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      height:170,
      child:Padding(
        padding: EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[
            Text("Partly cloudy conditions from 03:00-09:00 with\nmostly cloudy conditions expected at 09:00",
              style: TextStyle(
                fontSize: 15,
                color: Colors.grey.shade200,
              ),
            ),
            buildDivider(),
            Container(
              margin:EdgeInsets.only(top:8,bottom: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  buildHourly1(),
                  buildHourly2(),
                  buildHourly3(),
                  buildHourly4(),
                  buildHourly5(),
                  buildHourly6(),
                ],
              ),
            )
          ],
        ),
      )
  );
}

Widget buildHourly1(){
  return Column(
    children: <Widget>[
      Text("Now",
        style: TextStyle(
            color:Colors.white
        ),
      ),
      IconButton(
        icon: Icon(
          Icons.nights_stay,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("24°",
        style: TextStyle(
            color:Colors.white
        ),),
    ],
  );
}

Widget buildHourly2() {
  return Column(
    children: <Widget>[
      Text("00",
        style: TextStyle(
            color: Colors.white
        ),
      ),
      IconButton(
        icon: Icon(
          Icons.nights_stay,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("24°",
        style: TextStyle(
            color: Colors.white
        ),),
    ],
  );
}


Widget buildHourly3() {
  return Column(
    children: <Widget>[
      Text("01",
        style: TextStyle(
            color: Colors.white
        ),
      ),
      IconButton(
        icon: Icon(
          Icons.nights_stay,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("23°",
        style: TextStyle(
            color: Colors.white
        ),),
    ],
  );
}


Widget buildHourly4() {
  return Column(
    children: <Widget>[
      Text("02",
        style: TextStyle(
            color: Colors.white
        ),
      ),
      IconButton(
        icon: Icon(
          Icons.nights_stay,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("23°",
        style: TextStyle(
            color: Colors.white
        ),),
    ],
  );
}


Widget buildHourly5() {
  return Column(
    children: <Widget>[
      Text("03",
        style: TextStyle(
            color: Colors.white
        ),
      ),
      IconButton(
        icon: Icon(
          Icons.nights_stay,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("22°",
        style: TextStyle(
            color: Colors.white
        ),),
    ],
  );
}


Widget buildHourly6() {
  return Column(
    children: <Widget>[
      Text("04",
        style: TextStyle(
            color: Colors.white
        ),
      ),
      IconButton(
        icon: Icon(
          Icons.nights_stay,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("22°",
        style: TextStyle(
            color: Colors.white
        ),),
    ],
  );
}

Widget build10Day() {
  return Container(
      margin: EdgeInsets.all(5),
      decoration: ShapeDecoration(
        color:Colors.black38,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      height:700,
      child:Column(
        children: <Widget>[
          build10DayTitle(),
          buildDivider(),
          build10Day1(),
          buildDivider(),
          build10Day2(),
          buildDivider(),
          build10Day3(),
          buildDivider(),
          build10Day4(),
          buildDivider(),
          build10Day5(),
          buildDivider(),
          build10Day6(),
          buildDivider(),
          build10Day7(),
          buildDivider(),
          build10Day8(),
          buildDivider(),
          build10Day9(),
          buildDivider(),
          build10Day10(),





        ],
      )
  );
}

Widget build10DayTitle() {
  return Row(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.calendar_month_outlined,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("10-DAY FORCAST",style: TextStyle(color: Colors.white),
      )
    ],
  );
}

Widget buildDivider(){
  return Divider(
    color: Colors.grey,
  );
}

Widget build10Day1() {
  return Container(
    margin: EdgeInsets.all(5),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text("Today",style: TextStyle(color: Colors.white),
        ),
        IconButton(
          icon: Icon(
            Icons.nights_stay,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text("21°" ,style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w800),
        ),
        IconButton(onPressed: () {},
          icon: Icon(
              Icons.linear_scale_outlined,
              color:Colors.amberAccent
          ),
        ),
        Text("32°",style: TextStyle(color: Colors.white,fontWeight: FontWeight.w800),
        )
      ],
    ),
  );
}

Widget build10Day2() {
  return Container(
    margin: EdgeInsets.all(5),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text("Thu",style: TextStyle(color: Colors.white),
        ),
        IconButton(
          icon: Icon(
            Icons.nights_stay,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text("21°" ,style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w800),
        ),
        IconButton(onPressed: () {},
          icon: Icon(
              Icons.linear_scale_outlined,
              color:Colors.amberAccent
          ),
        ),
        Text("32°",style: TextStyle(color: Colors.white,fontWeight: FontWeight.w800),
        )
      ],
    ),
  );
}

Widget build10Day3() {
  return Container(
    margin: EdgeInsets.all(5),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text("Fri",style: TextStyle(color: Colors.white),
        ),
        IconButton(
          icon: Icon(
            Icons.nights_stay,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text("21°" ,style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w800),
        ),
        IconButton(onPressed: () {},
          icon: Icon(
              Icons.linear_scale_outlined,
              color:Colors.amberAccent
          ),
        ),
        Text("29°",style: TextStyle(color: Colors.white,fontWeight: FontWeight.w800),
        )
      ],
    ),
  );
}

Widget build10Day4() {
  return Container(
    margin: EdgeInsets.all(5),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text("Sat",style: TextStyle(color: Colors.white),
        ),
        IconButton(
          icon: Icon(
            Icons.nights_stay,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text("20°" ,style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w800),
        ),
        IconButton(onPressed: () {},
          icon: Icon(
              Icons.linear_scale_outlined,
              color:Colors.amberAccent
          ),
        ),
        Text("26°",style: TextStyle(color: Colors.white,fontWeight: FontWeight.w800),
        )
      ],
    ),
  );
}

Widget build10Day5() {
  return Container(
    margin: EdgeInsets.all(5),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text("Sun",style: TextStyle(color: Colors.white),
        ),
        IconButton(
          icon: Icon(
            Icons.cloud,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text("19°" ,style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w800),
        ),
        IconButton(onPressed: () {},
          icon: Icon(
              Icons.linear_scale_outlined,
              color:Colors.amberAccent
          ),
        ),
        Text("30°",style: TextStyle(color: Colors.white,fontWeight: FontWeight.w800),
        )
      ],
    ),
  );
}

Widget build10Day6() {
  return Container(
    margin: EdgeInsets.all(5),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text("Mon",style: TextStyle(color: Colors.white),
        ),
        IconButton(
          icon: Icon(
            Icons.cloud,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text("21°" ,style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w800),
        ),
        IconButton(onPressed: () {},
          icon: Icon(
              Icons.linear_scale_outlined,
              color:Colors.amberAccent
          ),
        ),
        Text("32°",style: TextStyle(color: Colors.white,fontWeight: FontWeight.w800),
        )
      ],
    ),
  );
}

Widget build10Day7() {
  return Container(
    margin: EdgeInsets.all(5),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text("Tue",style: TextStyle(color: Colors.white),
        ),
        IconButton(
          icon: Icon(
            Icons.sunny,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text("22°" ,style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w800),
        ),
        IconButton(onPressed: () {},
          icon: Icon(
              Icons.linear_scale_outlined,
              color:Colors.amberAccent
          ),
        ),
        Text("33°",style: TextStyle(color: Colors.white,fontWeight: FontWeight.w800),
        )
      ],
    ),
  );
}

Widget build10Day8() {
  return Container(
    margin: EdgeInsets.all(5),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text("Wed",style: TextStyle(color: Colors.white),
        ),
        IconButton(
          icon: Icon(
            Icons.cloud,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text("22°" ,style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w800),
        ),
        IconButton(onPressed: () {},
          icon: Icon(
              Icons.linear_scale_outlined,
              color:Colors.amberAccent
          ),
        ),
        Text("32°",style: TextStyle(color: Colors.white,fontWeight: FontWeight.w800),
        )
      ],
    ),
  );
}

Widget build10Day9() {
  return Container(
    margin: EdgeInsets.all(5),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text("Thu",style: TextStyle(color: Colors.white),
        ),
        IconButton(
          icon: Icon(
            Icons.sunny,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text("22°" ,style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w800),
        ),
        IconButton(onPressed: () {},
          icon: Icon(
              Icons.linear_scale_outlined,
              color:Colors.amberAccent
          ),
        ),
        Text("30°",style: TextStyle(color: Colors.white,fontWeight: FontWeight.w800),
        )
      ],
    ),
  );
}

Widget build10Day10() {
  return Container(
    margin: EdgeInsets.all(5),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text("Fri",style: TextStyle(color: Colors.white),
        ),
        IconButton(
          icon: Icon(
            Icons.sunny,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        Text("22°" ,style: TextStyle(color: Colors.grey,fontWeight: FontWeight.w800),
        ),
        IconButton(onPressed: () {},
          icon: Icon(
              Icons.linear_scale_outlined,
              color:Colors.amberAccent
          ),
        ),
        Text("31°",style: TextStyle(color: Colors.white,fontWeight: FontWeight.w800),
        )
      ],
    ),
  );
}