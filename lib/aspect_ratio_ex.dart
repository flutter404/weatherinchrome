import 'package:flutter/material.dart';

void main() {
  runApp(const Aspectio());
}

class Aspectio extends StatelessWidget {
  const Aspectio({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext buildContext) {
    return Center(
      child: Container(
        height: 200,
        width: 300,
        color: Colors.green,
        child: const FlutterLogo(),
      ),
    );
  }
}